#ifndef DEVICESELECTIONPAGE_H
#define DEVICESELECTIONPAGE_H

#include "ui_deviceSelection.h"


class DeviceSelectionPage final
{
public:
    DeviceSelectionPage();
    ~DeviceSelectionPage();

    void setupUi(QWidget *widget);

    void selectZebra();
    void selectWasp();

private:
    Ui_DeviceSelectionPage *deviceSelectionPage;
};


#endif // DEVICESELECTIONPAGE_H
