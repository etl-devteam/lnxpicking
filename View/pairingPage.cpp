#include "pairingPage.h"


PairingPage::PairingPage() :
    pairingPage{ new Ui_PairingPage }
{
}


PairingPage::~PairingPage()
{
    delete pairingPage;
    pairingPage = nullptr;
}


void PairingPage::setupUi(QWidget *widget)
{
    pairingPage->setupUi(widget);
}


void PairingPage::setWasp()
{
    pairingPage->labelWaspOrZebra->setText("WASP");
    pairingPage->labelWaspOrZebraImg->setPixmap(QPixmap(":/products/products/wasp_bluetooth.png"));
}


void PairingPage::setZebra()
{
    pairingPage->labelWaspOrZebra->setText("ZEBRA");
    pairingPage->labelWaspOrZebraImg->setPixmap(QPixmap(":/products/products/zebra_bluetooth.png"));
}
