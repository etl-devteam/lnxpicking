#include <QPropertyAnimation>

#include "deviceSelectionPage.h"


DeviceSelectionPage::DeviceSelectionPage() :
    deviceSelectionPage{ new Ui_DeviceSelectionPage }
{
}


DeviceSelectionPage::~DeviceSelectionPage()
{
    delete deviceSelectionPage;
    deviceSelectionPage = nullptr;
}


void DeviceSelectionPage::setupUi(QWidget *widget)
{
    deviceSelectionPage->setupUi(widget);
}


void DeviceSelectionPage::selectZebra()
{
    QPropertyAnimation *animation = new QPropertyAnimation(deviceSelectionPage->labelTarget, "geometry");

    animation->setDuration(250);
    animation->setStartValue(deviceSelectionPage->labelTarget->geometry());
    animation->setEndValue(QRect(40, 30, 81, 71));

    deviceSelectionPage->labelTarget->raise();

    animation->start(QAbstractAnimation::DeleteWhenStopped);
}


void DeviceSelectionPage::selectWasp()
{
    QPropertyAnimation *animation = new QPropertyAnimation(deviceSelectionPage->labelTarget, "geometry");

    animation->setDuration(250);
    animation->setStartValue(deviceSelectionPage->labelTarget->geometry());
    animation->setEndValue(QRect(290, 30, 81, 71));

    deviceSelectionPage->labelTarget->raise();

    animation->start(QAbstractAnimation::DeleteWhenStopped);
}
