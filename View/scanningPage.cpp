#include "scanningPage.h"

#include <QDebug>


ScanningPage::ScanningPage() :
    scanningPage{ new Ui_ScanningPage },
    targetLabel { new QLabel          }
{
}


ScanningPage::~ScanningPage()
{
    delete scanningPage;
    scanningPage = nullptr;
}


void ScanningPage::setupUi(QWidget *widget)
{
    scanningPage->setupUi(widget);
}


void ScanningPage::startScanning(const QString &firstTarget)
{
    buildTable();
    hideAllTargets();

    targetLabel->setGeometry(table.value(firstTarget)->geometry());

    QPixmap targetPixmap(":/icons/icons/mirino_3.png");
    targetLabel->setPixmap(targetPixmap.scaled(targetLabel->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));

    targetLabel->show();

    scanningPage->labelCurrentItem->hide();
    scanningPage->labelItemString->setText("Scan the first item");
}


void ScanningPage::hideAllTargets()
{
    scanningPage->labelHair    ->hide();
    scanningPage->labelLock    ->hide();
    scanningPage->labelKeyboard->hide();
    scanningPage->labelBrush   ->hide();
    scanningPage->labelMouse   ->hide();
    scanningPage->labelCoffee  ->hide();
    scanningPage->labelLaptop  ->hide();
    scanningPage->labelWrench  ->hide();
    scanningPage->labelCocktail->hide();

    targetLabel->hide();
}


void ScanningPage::buildTable()
{
    table =
    {
        {"CD-03-02", scanningPage->labelBrush},
        {"CD-03-03", scanningPage->labelLaptop},
        {"CD-04-02", scanningPage->labelKeyboard},
        {"CD-04-03", scanningPage->labelMouse},
        {"CD-05-02", scanningPage->labelHair},
        {"CD-05-03", scanningPage->labelLock},
        {"CD-03-04", scanningPage->labelCocktail},
        {"CD-04-04", scanningPage->labelWrench},
        {"CD-05-04", scanningPage->labelCoffee}
    };
}


void ScanningPage::moveTarget(const QString &scannedMessage, const QString &nextTarget)
{
    targetLabel->setGeometry(table.value(nextTarget)->geometry());

    scanningPage->labelCurrentItem->setPixmap(table.value(scannedMessage)->pixmap()->scaled(scanningPage->labelCurrentItem->size(),
                                                                                            Qt::KeepAspectRatio, Qt::SmoothTransformation));
    scanningPage->labelCurrentItem->show();

    scanningPage->labelItemString->setText("item scanned: " + scannedMessage);
}


void ScanningPage::scanningDone()
{
    targetLabel->hide();
}
