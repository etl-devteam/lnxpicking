#ifndef SCANNINGPAGE_H
#define SCANNINGPAGE_H

#include "ui_scanning.h"


class ScanningPage final
{
public:
    ScanningPage();
    ~ScanningPage();

    void setupUi(QWidget *widget);

    void startScanning(const QString &firstTarget);
    void scanningDone();

    void moveTarget(const QString &scannedMessage, const QString &nextTarget);

private:
    Ui_ScanningPage *scanningPage;

    QMap<QString, QLabel *> table;  // key is string, value is label

    QScopedPointer<QLabel> targetLabel;  // target picture to move around when scanning

    void hideAllTargets();
    void buildTable();
};


#endif // SCANNINGPAGE_H
