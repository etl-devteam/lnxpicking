#ifndef DEVICECONNECTIONPAGE_H
#define DEVICECONNECTIONPAGE_H

#include "ui_connecting.h"


class DeviceConnectionPage final
{
public:
    DeviceConnectionPage();
    ~DeviceConnectionPage();

    void setupUi(QWidget *widget);

    void setImageToWasp();
    void setImageToZebra();

private:
    Ui_ConnectingPage *deviceConnectionPage;
};


#endif // DEVICECONNECTIONPAGE_H
