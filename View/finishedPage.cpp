#include "finishedPage.h"


FinishedPage::FinishedPage() :
    finishedPage{ new Ui_finishedPage }
{
}


FinishedPage::~FinishedPage()
{
    delete finishedPage;
    finishedPage = nullptr;
}


void FinishedPage::setupUi(QWidget *widget)
{
    finishedPage->setupUi(widget);
}
