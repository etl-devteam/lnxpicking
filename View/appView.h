#ifndef APPVIEW_H
#define APPVIEW_H

#include <VisionAR/Framework/abstractVisionARView.h>

#include "welcomePage.h"
#include "deviceSelectionPage.h"
#include "deviceConnectionPage.h"
#include "scanningPage.h"
#include "finishedPage.h"
#include "pairingPage.h"


class AppView final : public AbstractVisionARView
{
    Q_OBJECT

public:
    AppView();

private:
    QScopedPointer<WelcomePage>          welcomePage;
    QScopedPointer<DeviceSelectionPage>  deviceSelectionPage;
    QScopedPointer<DeviceConnectionPage> deviceConnectionPage;
    QScopedPointer<PairingPage>          pairingPage;
    QScopedPointer<ScanningPage>         scanningPage;
    QScopedPointer<FinishedPage>         finishedPage;

public slots:
    void selectZebra();
    void selectWasp();
    void chooseWasp();
    void chooseZebra();
    void scanningDone();
    void startScanning(const QString &firstTarget);
    void moveTarget(const QString &scannedMessage, const QString &nextTarget);
};


#endif // APPVIEW_H
