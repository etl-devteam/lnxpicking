#include "appView.h"


AppView::AppView() :
    AbstractVisionARView(),
    welcomePage         { new WelcomePage          },
    deviceSelectionPage { new DeviceSelectionPage  },
    deviceConnectionPage{ new DeviceConnectionPage },
    pairingPage         { new PairingPage          },
    scanningPage        { new ScanningPage         },
    finishedPage        { new FinishedPage         }
{
    // the order in which you "register window"s here
    // matches the number you pass in "showWindow(int)"

    registerWindow(welcomePage.data());
    registerWindow(deviceSelectionPage.data());
    registerWindow(pairingPage.data());
    registerWindow(deviceConnectionPage.data());
    registerWindow(scanningPage.data());
    registerWindow(finishedPage.data());
}


void AppView::selectWasp()
{
    deviceSelectionPage->selectWasp();
    deviceConnectionPage->setImageToWasp();
}


void AppView::startScanning(const QString &firstTarget)
{
    scanningPage->startScanning(firstTarget);
}


void AppView::selectZebra()
{
    deviceSelectionPage->selectZebra();
    deviceConnectionPage->setImageToZebra();
}


void AppView::chooseWasp()
{
    pairingPage->setWasp();
}


void AppView::chooseZebra()
{
    pairingPage->setZebra();
}


void AppView::moveTarget(const QString &scannedMessage, const QString &nextTarget)
{
    scanningPage->moveTarget(scannedMessage, nextTarget);
}


void AppView::scanningDone()
{
    scanningPage->scanningDone();
}
