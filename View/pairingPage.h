#ifndef PAIRINGPAGE_H
#define PAIRINGPAGE_H

#include "ui_pairing.h"


class PairingPage final
{
public:
    PairingPage();
    ~PairingPage();

    void setupUi(QWidget *widget);

    void setWasp();
    void setZebra();

private:
    Ui_PairingPage *pairingPage;
};


#endif // PAIRINGPAGE_H
