#include "welcomePage.h"


WelcomePage::WelcomePage() :
    welcomePage{ new Ui_Welcome_page }
{
}


WelcomePage::~WelcomePage()
{
    delete welcomePage;
    welcomePage = nullptr;
}


void WelcomePage::setupUi(QWidget *widget)
{
    welcomePage->setupUi(widget);
}
