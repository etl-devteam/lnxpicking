#ifndef FINISHEDPAGE_H
#define FINISHEDPAGE_H

#include "ui_finished.h"


class FinishedPage final
{
public:
    FinishedPage();
    ~FinishedPage();

    void setupUi(QWidget *widget);

private:
    Ui_finishedPage *finishedPage;
};


#endif // FINISHEDPAGE_H
