#include "deviceConnectionPage.h"


DeviceConnectionPage::DeviceConnectionPage() :
    deviceConnectionPage{ new Ui_ConnectingPage }
{
}


DeviceConnectionPage::~DeviceConnectionPage()
{
    delete deviceConnectionPage;
    deviceConnectionPage = nullptr;
}


void DeviceConnectionPage::setupUi(QWidget *widget)
{
    deviceConnectionPage->setupUi(widget);
}


void DeviceConnectionPage::setImageToWasp()
{
    deviceConnectionPage->labelImage->setPixmap(QPixmap(":/products/products/wasp_bluetooth.png"));
}


void DeviceConnectionPage::setImageToZebra()
{
    deviceConnectionPage->labelImage->setPixmap(QPixmap(":/products/products/zebra_bluetooth.png"));
}
