#include <QApplication>
#include <QScopedPointer>

#include "Controller/appController.h"


int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> a(new QApplication(argc, argv));

    AppController::startCheckingGlasses();

    QScopedPointer<AppModel> model(new AppModel);
    QScopedPointer<AppView>  view (new AppView);

    AppController controller(view.data(), model.data(), "PickingDemo/PickingDemo");

    return a->exec();
}
