QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = PickingDemo
TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Controller/appController.cpp \
    Model/appModel.cpp \
    Model/bluetoothManager.cpp \
    Model/gridManager.cpp \
    View/appView.cpp \
    View/deviceConnectionPage.cpp \
    View/deviceSelectionPage.cpp \
    View/finishedPage.cpp \
    View/pairingPage.cpp \
    View/scanningPage.cpp \
    View/welcomePage.cpp \
    main.cpp

HEADERS += \
    Controller/appController.h \
    Model/appModel.h \
    Model/bluetoothManager.h \
    Model/gridManager.h \
    View/appView.h \
    View/deviceConnectionPage.h \
    View/deviceSelectionPage.h \
    View/finishedPage.h \
    View/pairingPage.h \
    View/scanningPage.h \
    View/welcomePage.h

FORMS += \
    View/pairing.ui \
    View/welcome.ui \
    View/connecting.ui \
    View/deviceSelection.ui \
    View/finished.ui \
    View/scanning.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -lBtnsEventGen2 -lGlassesCheck -lTouchEventsGen2 -lVisionARAlertFactory \
        -lAbstractVisionARController -lAbstractVisionARModel -lAbstractVisionARView \
        -lThreadObjInterface -lBtnsInterface -lBatteryManager -lAlertsManager -lLauncherAdapterInterface -lTouchInterface

RESOURCES += resource.qrc
