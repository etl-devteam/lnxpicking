#ifndef BLUETOOTHMANAGER_H
#define BLUETOOTHMANAGER_H

#include <QObject>


QT_FORWARD_DECLARE_CLASS(QTimer)
QT_FORWARD_DECLARE_CLASS(QProcess)


enum class DeviceOption : short {
    WASP = 1, ZEBRA, NONE,
};


class BluetoothManager final : public QObject
{
    Q_OBJECT

public:
    BluetoothManager(DeviceOption device);

    ~BluetoothManager();

    void startTimer();
    void stopTimer();

    bool connectDevice();

    void setDevice(DeviceOption device);

private:

    static constexpr int TIMER_MILLISECONDS  { 400  };
    static constexpr int PROCESS_MILLISECONDS{ 5000 };
    static constexpr int BUFFER_SIZE         { 128  };

    static constexpr char const * const MAC_ZEBRA{ "94:FB:29:64:AF:A3" };
    static constexpr char const * const MAC_WASP { "00:1C:97:17:18:F4" };

    static constexpr char const * const  RFCOMM_ZEBRA{ "/dev/rfcomm2" };
    static constexpr char const * const  RFCOMM_WASP { "/dev/rfcomm0" };

    QString address;
    QString rfcommFileName;
    QString scanToEmit;

    QScopedPointer<QTimer> timer;

    QProcess *process;

signals:
    void messageReceivedSig(QString message);

private slots:
    void checkForNewScan();
};


#endif
