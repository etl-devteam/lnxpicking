#include <algorithm>
#include <random>

#include "gridManager.h"


GridManager::GridManager() :
    barCodes{ "CD-03-02",
              "CD-03-03",
              "CD-04-02",
              "CD-04-03",
              "CD-05-02",
              "CD-05-03",
              "CD-03-04",
              "CD-04-04",
              "CD-05-04" }
{
}


void GridManager::initializeBarCodes()
{
    std::shuffle(barCodes.begin(), barCodes.end(), std::default_random_engine{ std::random_device{}() });
    currIterator = barCodes.cbegin();
}


//  return true if finished scanning
bool GridManager::messageReceived(const QString &message)
{
    // check if finished scanning and did not start again
    if (currIterator == barCodes.cend()) return false;

    if (*currIterator == message)
    {
        if (++currIterator == barCodes.cend())
        {
            return true;
        }

         emit moveTarget(message, *currIterator);
    }

    return false;
}


const QString &GridManager::firstTarget() const
{
    return barCodes.first();
}
