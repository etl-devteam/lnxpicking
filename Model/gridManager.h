#ifndef GRIDMANAGER_H
#define GRIDMANAGER_H

#include <QObject>
#include <QVector>


class GridManager final : public QObject
{
    Q_OBJECT

public:
    GridManager();

    void initializeBarCodes();

    bool messageReceived(const QString &message);
    const QString &firstTarget() const;

private:
    QVector<QString> barCodes;
    QVector<QString>::const_iterator currIterator;

signals:
    void moveTarget(const QString &scannedMessage, const QString &nextTarget);
};


#endif // GRIDMANAGER_H
