#include <QTimer>
#include <QProcess>
#include <QDebug>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "bluetoothManager.h"


BluetoothManager::BluetoothManager(DeviceOption device) :
    QObject(),
    address       { device == DeviceOption::WASP ? MAC_WASP    : MAC_ZEBRA    },
    rfcommFileName{ device == DeviceOption::WASP ? RFCOMM_WASP : RFCOMM_ZEBRA },
    timer         { new QTimer(this) },
    process       { nullptr }
{
    connect(timer.data(), &QTimer::timeout, this, &BluetoothManager::checkForNewScan);
}


BluetoothManager::~BluetoothManager()
{
    QProcess::execute("/bin/sh", {"-c", "rfcomm release all"});

    if (process != nullptr) delete process;
    process = nullptr;

    stopTimer();
}


bool BluetoothManager::connectDevice()
{
    if (process != nullptr) delete process;
    process = new QProcess(this);

    process->start("/bin/sh", {"-c", "rfcomm connect " + rfcommFileName + " " + address});

    if (!process->waitForStarted(PROCESS_MILLISECONDS))  return false;

    process->waitForFinished(PROCESS_MILLISECONDS);

    return true;
}


void BluetoothManager::checkForNewScan()
{
    const int fd = open(rfcommFileName.toStdString().c_str(), O_RDONLY);

    if (fd >= 0)
    {
        char buf[BUFFER_SIZE];
        const auto bytesRead = read(fd, buf, sizeof buf);

        if (bytesRead < 0)
        {
            qDebug() << "problem with read sys call";
        }
        else
        {
            buf[bytesRead] = '\0';  // insert null terminator

            //  not the best solution, but would do the job
            if (address == MAC_WASP)
            {
                scanToEmit.append(buf);

                if (scanToEmit.endsWith("\n\n"))
                {
                    qDebug() << "about to emit (in wasp) message -> " << scanToEmit.trimmed();
                    emit messageReceivedSig(scanToEmit.trimmed());
                    scanToEmit.clear();
                }
            }
            else if (address == MAC_ZEBRA)
            {
                qDebug() << "about to emit (in zebra) message -> " << QString(buf);
                emit messageReceivedSig(buf);
            }
        }

        close(fd);
    }
}


void BluetoothManager::setDevice(DeviceOption device)
{
    address        = device == DeviceOption::WASP ? MAC_WASP    : MAC_ZEBRA;
    rfcommFileName = device == DeviceOption::WASP ? RFCOMM_WASP : RFCOMM_ZEBRA;
}


void BluetoothManager::startTimer()
{
    timer->start(TIMER_MILLISECONDS);
}


void BluetoothManager::stopTimer()
{
    timer->stop();
}
