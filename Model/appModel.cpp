#include <QDebug>

#include "appModel.h"
#include "gridManager.h"


AppModel::AppModel() :
    AbstractVisionARModel(),
    status          { AppStatus::WELCOME },
    device          { DeviceOption::NONE },
    bluetoothManager{ nullptr            },
    gridManager     { new GridManager    }
{
}


AppModel::~AppModel()
{
    if (bluetoothManager != nullptr)
    {
        delete bluetoothManager;
        bluetoothManager = nullptr;
    }
}


void AppModel::runModel()
{
    connect(gridManager.data(), &GridManager::moveTarget, this, &AppModel::moveTarget);

    emit showWindow(FIRST_WINDOW);
}


void AppModel::backReleased(int /*timePressed*/)
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit newAlert(1, "QuestionCloseApp", "Do you wish to return to the Launcher?");
        break;

    case AppStatus::DEVICE_SELECTION:
        emit showWindow(FIRST_WINDOW);
        status = AppStatus::WELCOME;
        break;

    case AppStatus::PAIRING:
    case AppStatus::CONNECTING:
    case AppStatus::SCANNING:

        if (bluetoothManager != nullptr) delete bluetoothManager;
        bluetoothManager = nullptr;

        emit showWindow(SECOND_WINDOW);
        emit scanningDoneSig();

        status = AppStatus::DEVICE_SELECTION;
        break;

    case AppStatus::FINISHED:
        break;

    default:
        qDebug() << "moriremo tutti\n";
        break;
    }
}


void AppModel::okPressed()
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::DEVICE_SELECTION;
        break;

    case AppStatus::DEVICE_SELECTION: break;

    case AppStatus::PAIRING:
        if (device != DeviceOption::NONE)
        {
            emit showWindow(FOURTH_WINDOW);
            status = AppStatus::CONNECTING;

            if (bluetoothManager == nullptr)
            {
                bluetoothManager = new BluetoothManager(device);
            }
            else
            {
                bluetoothManager->setDevice(device);
            }

            connect(bluetoothManager, &BluetoothManager::messageReceivedSig, this, &AppModel::messageReceived);

            bluetoothManager->startTimer();

            if (bluetoothManager->connectDevice())
            {
                gridManager->initializeBarCodes();

                clientConnected();
            }
        }

    case AppStatus::CONNECTING: break;

    case AppStatus::SCANNING:   break;

    case AppStatus::FINISHED:

        emit showWindow(SECOND_WINDOW);
        status = AppStatus::DEVICE_SELECTION;

        break;

    default:
        qDebug() << "moriremo tutti\n";
        break;
    }
}


void AppModel::doubleTap()
{
    if (status == AppStatus::DEVICE_SELECTION && device != DeviceOption::NONE)
    {
        if (device == DeviceOption::WASP)
        {
            emit chooseWasp();
        }
        else
        {
            emit chooseZebra();
        }

        emit showWindow(THIRD_WINDOW);
        status = AppStatus::PAIRING;
    }
}


void AppModel::swipeForward()
{
    if (status == AppStatus::DEVICE_SELECTION && device != DeviceOption::WASP)
    {
        device = DeviceOption::WASP;
        emit selectWasp();
    }
}


void AppModel::swipeBackward()
{
    if (status == AppStatus::DEVICE_SELECTION && device != DeviceOption::ZEBRA)
    {
        device = DeviceOption::ZEBRA;
        emit selectZebra();
    }
}


void AppModel::manageAlertsEnd(QString id, bool value)
{
    if (status == AppStatus::WELCOME && id == "QuestionCloseApp" && value)
    {
        emit endApplication();
    }
    else if (id == "AttentionClientDisconnected")
    {
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::DEVICE_SELECTION;
    }
}


void AppModel::clientConnected()
{
    emit showWindow(FIFTH_WINDOW);
    emit startScanning(gridManager->firstTarget());

    status = AppStatus::SCANNING;
}


void AppModel::messageReceived(QString message)
{
    if (gridManager->messageReceived(message))
    {
        emit scanningDoneSig();
        emit showWindow(SIXTH_WINDOW);

        status = AppStatus::FINISHED;

        bluetoothManager->stopTimer();
    }
}


void AppModel::moveTarget(const QString &scannedMessage, const QString &nextTarget)
{
    emit moveTargetSig(scannedMessage, nextTarget);
}
