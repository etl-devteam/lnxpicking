#ifndef APPMODEL_H
#define APPMODEL_H

#include <VisionAR/Framework/abstractVisionARModel.h>

#include "bluetoothManager.h"

// forward declaration
class GridManager;


class AppModel final : public AbstractVisionARModel
{
    Q_OBJECT

public:
    AppModel();
    ~AppModel();

private:

    enum : short {
        FIRST_WINDOW = 1, SECOND_WINDOW, THIRD_WINDOW, FOURTH_WINDOW, FIFTH_WINDOW, SIXTH_WINDOW,
    };

    enum class AppStatus : short {
        WELCOME = 1, DEVICE_SELECTION, PAIRING, CONNECTING, SCANNING, FINISHED,
    };

    AppStatus    status;
    DeviceOption device;

    BluetoothManager *bluetoothManager;

    QScopedPointer<GridManager> gridManager;

    void runModel()      override;
    void backPressed()   override {}
    void okPressed()     override;
    void doubleTap()     override;
    void swipeForward()  override;
    void swipeBackward() override;

    void backReleased(    int  timePressed   )     override;
    void okReleased(      int /*timePressed*/ = 0) override {}
    void forwardReleased( int /*timePressed*/)     override {}
    void backwardReleased(int /*timePressed*/)     override {}
    void forwardPressed()                          override {}
    void backwardPressed()                         override {}
    void singleTap()                               override {}

    void manageAlertsEnd(QString id, bool value) override;

    void clientConnected();

private slots:
    void messageReceived(QString message);
    void moveTarget(const QString &scannedMessage, const QString &nextTarget);

signals:
    void selectWasp();
    void selectZebra();
    void chooseWasp();
    void chooseZebra();
    void startScanning(const QString &firstTarget);
    void moveTargetSig(const QString &scannedMessage, const QString &nextTarget);
    void scanningDoneSig();
};


#endif // APPMODEL_H
