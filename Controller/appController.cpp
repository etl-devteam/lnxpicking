#include "appController.h"

#include <QDebug>


AppController::AppController(AppView *view, AppModel *model, QString app_relative_path) :
    AbstractVisionARController(view, model, app_relative_path)
{
}


void AppController::connections()
{
    connect((AppModel*)model, SIGNAL(selectWasp()),                   (AppView*)view, SLOT(selectWasp()));
    connect((AppModel*)model, SIGNAL(selectZebra()),                  (AppView*)view, SLOT(selectZebra()));
    connect((AppModel*)model, SIGNAL(chooseWasp()),                   (AppView*)view, SLOT(chooseWasp()));
    connect((AppModel*)model, SIGNAL(chooseZebra()),                  (AppView*)view, SLOT(chooseZebra()));

    connect((AppModel*)model, SIGNAL(startScanning(const QString &)), (AppView*)view, SLOT(startScanning(const QString &)));
    connect((AppModel*)model, SIGNAL(scanningDoneSig()),              (AppView*)view, SLOT(scanningDone()));

    connect((AppModel*)model, SIGNAL(moveTargetSig(const QString &, const QString &)),
            (AppView*)view,   SLOT(  moveTarget(   const QString &, const QString &)));
}


void AppController::endActions()
{
}
